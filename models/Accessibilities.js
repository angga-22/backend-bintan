const mongoose = require('mongoose');

const AccessibilitiesSchema = mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  subTitle: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  contact: {
    type: String,
    required: false
  },
  latLong: {
    type: Array,
    required: false
  },
  // contoh struktur latlong
  // {
  //   "lat": 123,
  //   "long": 123
  // }
  listItem: {
    type: Array,
    required: false
  }
  // didalam list item :
  // {
  //   "listTitle": "global bintan",
  //   "listDesc": "lorem ipsum",
  //   "listContact": 123123,
  //   "icon": "http://imgbb/123.id"
  // }
})

module.exports = mongoose.model('Accessibility', AccessibilitiesSchema)
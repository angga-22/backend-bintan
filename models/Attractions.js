const mongoose = require('mongoose');

const AttractionSchema = mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  icon: {
    type: String,
    required: true
  },
  isAccomodation: {
    type: Boolean,
    required: false
  }
})

module.exports = mongoose.model('Attraction', AttractionSchema)
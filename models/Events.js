const mongoose = require('mongoose');

const EventSchema = mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  subTitle: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  postponed: {
    type: String,
    required: false
  },
  eventHistory: {
    type: Array,
    required: true
  },
  // {
  //   "firstHeld": "22 08 1997",
  //   "lastHeld": "22 08 1997"
  // }
  website: {
    type: String,
    required: false
  },
  recommendedPlan: {
    type: Array,
    required: false
  }
  // "start": "22 08 1997"
  // "end": "22 08 1997"
})

module.exports = mongoose.model('Event', EventSchema)
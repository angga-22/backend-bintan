const mongoose = require('mongoose');

const LodgingSchema = mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  subTitle: {
    type: String,
    required: false
  },
  description: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  latLong: {
    type: Array,
    required: true
  },
  //
  // lat: {},
  // long: {}
  recommendActivities: {
    type: String,
    required: false
  },
  bestView: {
    type: String,
    required: false
  },
  contact: {
    type: String,
    required: true
  },
  website: {
    type: String,
    required: true
  },
  bestActivities: {
    type: Array,
    required: true
  },
  // bestActivities: [
  //   {
  //     label: {
  //       type: String,
  //       required: false
  //     },
  //     kindOf: {
  //       type: String,
  //       required: false
  //     },
  //     time: {
  //       type: String,
  //       required: false
  //     }
  //   }
  // ],
  nearbyAccomodation: {
    type: Array,
    required: false
  },
  // nearbyAccomodation: [
  //   {
  //     label: {
  //       type: String,
  //       required: true
  //     },
  //     place: {
  //       type: String,
  //       required: true
  //     },
  //     icon: {
  //       type: String,
  //       required: true
  //     }
  //   }
  // ],
  nearbyAttraction: {
    type: Array,
    required: false
  }
  // nearbyAttraction: [
  //   {
  //     label: {
  //       type: String,
  //       required: true
  //     },
  //     availability: {
  //       type: String,
  //       required: true
  //     },
  //     image: {
  //       type: String,
  //       required: true
  //     },
  //     description: {
  //       type: String,
  //       required: true
  //     }
  //   }
  // ]
})

module.exports = mongoose.model('Lodging', LodgingSchema)
const mongoose = require('mongoose');

const usefulNumberSchema = mongoose.Schema({
  title: {
    type: String,
    required: false
  },
  subTitle: {
    type: String,
    required: false
  },
  description: {
    type: String,
    required: false
  },
  image: {
    type: String,
    required: true
  },
  contact: {
    type: String,
    required: false
  },
  latLong: {
    type: Array,
    required: false
  },
  isMedical: {
    type: Boolean,
    required: true
  },
  medical: {
    type: Array,
    required: false
  },
  // "listTitle": "clinic tourism",
  // "listAddress": "jalan bla bla bla",
  // "listContact": "123123"
  // "icon": "http://"
  // "kindOf": "clinic"

})

module.exports = mongoose.model('usefulNumber', usefulNumberSchema)
const mongoose = require('mongoose');

const HeritageSchema = mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  availableHour: {
    type: String,
    required: true
  },
  subTitle: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  bestActivities: {
    type: Array,
    required: true
  },
  latLong: {
    type: Array,
    required: true
  },
  nearbyAccomodation: {
    type: Array,
    required: false
  },
  // "label": "bandar bentan",
  // "location": "pulau bintan",
  // "duration": "40 minutes",
  // "image": "https:// bla bla bla"
  nearbyAttraction: {
    type: Array,
    required: false
  },
  // "label": "tes",
  // "availabilityHours": "tes",
  // "desc": "bla bla bla",
  // "image": "http: bla bla bla"
  nearbyLodging: {
    type: Array,
    required: false
  }
  // "label": "tes",
  // "start": 4,
  // "location": "pulau bintan",
  // "image": "http: bla bla bla"
})

module.exports = mongoose.model('Heritage', HeritageSchema)
const express = require('express')
const router = express.Router()
const Heritage = require('../models/Heritages')


// get all post
router.get('/', async (req, res) => {
  try {
    const posts = await Heritage.find()
    res.json(posts)
  } catch (err) {
    res.json({ message: err })
  }
})

// submit post
router.post('/', async (req, res) => {
  const post = new Heritage({
    title: req.body.title,
    availableHour: req.body.availableHour,
    subTitle: req.body.subTitle,
    description: req.body.description,
    image: req.body.image,
    bestActivities: req.body.bestActivities,
    latLong: req.body.latLong,
    nearbyAccomodation: req.body.nearbyAccomodation,
    nearbyAttraction: req.body.nearbyAttraction,
    nearbyLodging: req.body.nearbyLodging,
  })
  try {
    const savedPost = await post.save()
    res.json(savedPost)
  } catch (err) {
    res.json({ message: err })
  }

})


//specific post
router.get('/:postId', async (req, res) => {

  try {
    const post = await Heritage.findById(req.params.postId)
    res.json(post)
  } catch (err) {
    res.json({ message: err } || 'failed to get detail')
  }
})

//delete post

router.delete('/:postId', async (req, res) => {
  try {
    const removedPost = await Heritage.deleteOne({ _id: req.params.postId })
    res.json(removedPost)
  } catch (err) {
    res.json({ message: err } || 'failed to delete details')
  }
})

//update post 
router.patch('/:postId', async (req, res) => {
  try {
    const updatePost = await Heritage.updateOne(
      { _id: req.params.postId },
      { $set: { title: req.body.title } }
    )
    res.json(updatePost)
  } catch (err) {
    res.json({ message: err })
  }
})

module.exports = router;
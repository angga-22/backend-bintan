const express = require('express')
const router = express.Router()
const UsefulNumber = require('../models/UsefulNumbers')


// get all post
router.get('/', async (req, res) => {
  try {
    const posts = await UsefulNumber.find()
    res.json(posts)
  } catch (err) {
    res.json({ message: err })
  }
})

// submit post
router.post('/', async (req, res) => {
  const post = new UsefulNumber({
    title: req.body.title,
    subTitle: req.body.subTitle,
    description: req.body.description,
    image: req.body.image,
    contact: req.body.contact,
    latLong: req.body.latLong,
    isMedical: req.body.isMedical,
    medical: req.body.medical
  })
  try {
    const savedPost = await post.save()
    res.json(savedPost)
  } catch (err) {
    res.json({ message: err })
  }

})


//specific post
router.get('/:postId', async (req, res) => {

  try {
    const post = await UsefulNumber.findById(req.params.postId)
    res.json(post)
  } catch (err) {
    res.json({ message: err } || 'failed to get detail')
  }
})

//delete post

router.delete('/:postId', async (req, res) => {
  try {
    const removedPost = await UsefulNumber.deleteOne({ _id: req.params.postId })
    res.json(removedPost)
  } catch (err) {
    res.json({ message: err } || 'failed to delete details')
  }
})

//update post 
router.patch('/:postId', async (req, res) => {
  try {
    const updatePost = await UsefulNumber.updateOne(
      { _id: req.params.postId },
      { $set: { title: req.body.title } }
    )
    res.json(updatePost)
  } catch (err) {
    res.json({ message: err })
  }
})

module.exports = router;
const express = require('express')
const router = express.Router()
const Event = require('../models/Events')


// get all post
router.get('/', async (req, res) => {
  try {
    const posts = await Event.find()
    res.json(posts)
  } catch (err) {
    res.json({ message: err })
  }
})

// submit post
router.post('/', async (req, res) => {
  const post = new Event({
    title: req.body.title,
    subTitle: req.body.subTitle,
    description: req.body.description,
    image: req.body.image,
    postponed: req.body.postponed,
    eventHistory: req.body.eventHistory,
    website: req.body.website,
    recommendedPlan: req.body.recommendedPlan
  })
  try {
    const savedPost = await post.save()
    res.json(savedPost)
  } catch (err) {
    res.json({ message: err })
  }

})


//specific post
router.get('/:postId', async (req, res) => {

  try {
    const post = await Event.findById(req.params.postId)
    res.json(post)
  } catch (err) {
    res.json({ message: err } || 'failed to get detail')
  }
})

//delete post

router.delete('/:postId', async (req, res) => {
  try {
    const removedPost = await Event.deleteOne({ _id: req.params.postId })
    res.json(removedPost)
  } catch (err) {
    res.json({ message: err } || 'failed to delete details')
  }
})

//update post 
router.patch('/:postId', async (req, res) => {
  try {
    const updatePost = await Event.updateOne(
      { _id: req.params.postId },
      { $set: { title: req.body.title } }
    )
    res.json(updatePost)
  } catch (err) {
    res.json({ message: err })
  }
})

module.exports = router;
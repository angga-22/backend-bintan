const express = require('express')
const router = express.Router()
const Lodging = require('../models/Lodgings')


// get all post
router.get('/', async (req, res) => {
  try {
    const posts = await Lodging.find()
    res.json(posts)
  } catch (err) {
    res.json({ message: err })
  }
})

// submit post
router.post('/', async (req, res) => {
  const post = new Lodging({
    title: req.body.title,
    subTitle: req.body.title,
    description: req.body.description,
    image: req.body.image,
    latLong: req.body.latLong,
    contact: req.body.contact,
    website: req.body.website,
    bestActivities: req.body.bestActivities,
    nearbyAccomodation: req.body.nearbyAccomodation,
    nearbyAttraction: req.body.nearbyAttraction,
    recommendActivities: req.body.recommendActivities,
    bestView: req.body.bestView
  })
  try {
    const savedPost = await post.save()
    res.json(savedPost)
  } catch (err) {
    res.json({ message: err })
  }

})


//specific post
router.get('/:postId', async (req, res) => {

  try {
    const post = await Lodging.findById(req.params.postId)
    res.json(post)
  } catch (err) {
    res.json({ message: err } || 'failed to get detail')
  }
})

//delete post

router.delete('/:postId', async (req, res) => {
  try {
    const removedPost = await Lodging.deleteOne({ _id: req.params.postId })
    res.json(removedPost)
  } catch (err) {
    res.json({ message: err } || 'failed to delete details')
  }
})

//update post 
router.patch('/:postId', async (req, res) => {
  try {
    const updatePost = await Lodging.updateOne(
      { _id: req.params.postId },
      { $set: { title: req.body.title } }
    )
    res.json(updatePost)
  } catch (err) {
    res.json({ message: err })
  }
})

module.exports = router;
const express = require('express')
const router = express.Router()
const Accessibility = require('../models/Accessibilities')


// get all post
router.get('/', async (req, res) => {
  try {
    const posts = await Accessibility.find()
    res.json(posts)
  } catch (err) {
    res.json({ message: err })
  }
})

// submit post
router.post('/', async (req, res) => {
  const post = new Accessibility({
    title: req.body.title,
    subTitle: req.body.subTitle,
    description: req.body.description,
    image: req.body.image,
    contact: req.body.contact,
    latLong: req.body.latLong,
    listItem: req.body.listItem
  })
  try {
    const savedPost = await post.save()
    res.json(savedPost)
  } catch (err) {
    res.json({ message: err })
  }

})


//specific post
router.get('/:postId', async (req, res) => {

  try {
    const post = await Accessibility.findById(req.params.postId)
    res.json(post)
  } catch (err) {
    res.json({ message: err } || 'failed to get detail')
  }
})

//delete post

router.delete('/:postId', async (req, res) => {
  try {
    const removedPost = await Accessibility.deleteOne({ _id: req.params.postId })
    res.json(removedPost)
  } catch (err) {
    res.json({ message: err } || 'failed to delete details')
  }
})

//update post 
router.patch('/:postId', async (req, res) => {
  try {
    const updatePost = await Accessibility.updateOne(
      { _id: req.params.postId },
      { $set: { title: req.body.title } }
    )
    res.json(updatePost)
  } catch (err) {
    res.json({ message: err })
  }
})

module.exports = router;
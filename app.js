const express = require('express')
const app = express();
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const cors = require('cors')
require('dotenv/config')

//middleware
app.use(cors())

app.use(bodyParser.json())

const attractionRoute = require('./routes/attraction')
const usefulNumberRoute = require('./routes/usefulNumber')
const accessibilityRoute = require('./routes/accessibility')
const lodgingRoute = require('./routes/lodging')
const eventRoute = require('./routes/event')
const natureRoute = require('./routes/nature')
const heritageRoute = require('./routes/heritage')
app.use('/attractions', attractionRoute)
app.use('/useful-numbers', usefulNumberRoute)
app.use('/accessibilities', accessibilityRoute)
app.use('/lodgings', lodgingRoute)
app.use('/events', eventRoute)
app.use('/natures', natureRoute)
app.use('/heritages', heritageRoute)


//connect to db

mongoose.connect(
  process.env.DB_CONNECTION,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  },
  () => console.log('connected to db'))

app.listen(3000);
